# English Language Learning (kaggle competition)

- [Link to the competition](https://www.kaggle.com/competitions/feedback-prize-english-language-learning/)

## Requirements

- Install python 3.10
- It's recommended to create a virtual environment for the project
- Install requirements with `pip install -r requirements.txt`

## Run

- Use `jupyter-lab` to start JupyterLab
- Jupyter notebooks are in the folder **notebooks**

## Kaggle notebook

- Alternatively you can copy and run this [notebook](https://www.kaggle.com/code/swarmy/english-language-learning-albert) from the kaggle.
- It's modified so it can be submitted as a solution to compute private score.
